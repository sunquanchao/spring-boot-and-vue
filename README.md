# Spring Boot with Vue.js

## tech stack

- Spring Boot v2
- Kotlin & Kotlin DSL for HTML
- yarn + parcel.js
- data mocking via wiremock

## start the application

```bash
mvn clean spring-boot:run
```

## run + watch Vue.js bundle build

```bash
# or npm
yarn bundle
yarn watch
```