package com.example.demo

import kotlinx.html.*
import kotlinx.html.dom.createHTMLDocument
import kotlinx.html.dom.serialize
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class MainPageController {

    @GetMapping("/")
    fun renderMainPage(): String {
        return renderInitialMarkup()
    }
}


fun renderInitialMarkup(): String =
        createHTMLDocument().html {

            head {
                meta(content = "width=device-width, initial-scale=1, shrink-to-fit=no", name = "viewport")
                title { +"Wishlist by Vue.js" }
                meta(charset = "utf-8")
            }

            body {
                h1 { +"spring-boot and vue" }
                div { id = "vue" }
                script(src = "/dist/bundle.js") {}
            }
        }.serialize(true)
