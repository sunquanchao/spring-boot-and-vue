import { shallowMount } from "@vue/test-utils";
import Tile from "./Tile";

xdescribe("Tile", () => {
    test("is a Vue instance", () => {
        const wrapper = shallowMount(Tile, {
            propsData: {
                id: 1,
                title: "title",
                url: "url",
                image: "image",
                category: "category",
                addedOn: "addedOn",
                price: "price"
            }
        });
        expect(wrapper.isVueInstance()).toBeTruthy();
        expect(wrapper.findAll("a")).toHaveLength(1);
        expect(wrapper.findAll("li")).toHaveLength(3);
    })
})