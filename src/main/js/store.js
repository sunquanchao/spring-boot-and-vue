import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const state = {
    owner: "",
    count: 0,
    items: []
}

const getters = {
    getOwner: state => {
        return state.owner
    },
    getCount: state => {
        return state.count
    },
    getItems: state => {
        return state.items
    }
}

const mutations = {
    setOwner (state, value) {
        state.owner = value
    },
    setCount (state, value) {
        state.count = value
    },
    setItems (state, value) {
        state.items = value
    }
}

export default new Vuex.Store({
    state,
    getters,
    mutations
});