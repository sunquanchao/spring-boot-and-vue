import "babel-polyfill";
import Vue from "vue";
import App from "./App.vue";

import store from "./store";

new Vue({
    el: "#vue",
    store,
    render: h => h(App)
});